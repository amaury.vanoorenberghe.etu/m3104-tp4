import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.time.*;
import java.time.temporal.*;

@WebServlet("/NouvelAn")
public class NouvelAn extends HttpServlet
{
	public void service( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		out.println("<head><title>Nouvel an</title>" );
		out.println("<META content=\"charset=UTF-8\"></head><body>" );

		out.println("<center>Nouvel an</center>");
		
		LocalDateTime today = LocalDateTime.now();
		LocalDateTime januaryFirst = LocalDateTime.of(today.getYear() + 1, Month.JANUARY, 1, 0, 0);
		Duration delay = Duration.between(today, januaryFirst);
		long seconds = delay.get(ChronoUnit.SECONDS);
	
		out.println(String.format("Il reste %d secondes avant le nouvel an...", seconds));
	}
}