import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/Palette")
public class Palette extends HttpServlet
{
	public void service( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		out.println("<head><title>Palette</title>" );
		out.println("<META content=\"charset=UTF-8\"></head><body>" );

		out.println("<center>Palette<table>");
		
		for (int x = 0; x < 16; ++x) {
			out.println("<tr>");
			
			for (int y = 0; y < 16; ++y) {
				String red = Integer.toHexString(16 - ((x + y) / 2) + 1);
				String blue = Integer.toHexString(x);
				String green = Integer.toHexString(y);
				out.println(String.format("<td style=\"width: 1em;height:1em;background: #%s%s%s\"></td>", red, green, blue));
			}
			
			out.println("</tr></center>");
		}
	
		out.println( "</table>");
	}
}